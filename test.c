#include <stdio.h>  // needed for size_t

#include <unistd.h>



typedef struct metadata {
  /* size_t is the return type of the sizeof operator. Since the size of an
   * object depends on the architecture and its implementation, size_t is used
   * to represent the maximum size of any object in the particular
   * implementation. size contains the size of the data object or the number of
   * free bytes
   */
  size_t size;
  struct metadata* next;
  struct metadata* prev; 
} metadata_t;

int main() {
	metadata_t* freelist = (metadata_t*) sbrk(1024);
	printf("address: 0x%x\n", freelist);
	metadata_t* next = freelist + 1;
	printf("address+1: 0x%x\n", next);
	printf("size metadata: %u\n", sizeof(metadata_t));
	return 0;
}





