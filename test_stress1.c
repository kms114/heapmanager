#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "dmm.h"

int main(int argc, char *argv[])
{
	int size;
	void *ptr[10];
	int i;

	/*
 	 * try mallocing four pieces, each 1/4 of total size
 	 */
	size = MAX_HEAP_SIZE/ 4;
    
	ptr[0] = dmalloc(size);
    printf("mallocing ptr[0]: 0x%x\n", ptr[0]);
	if(ptr[0] == NULL)
	{
		printf("malloc of ptr[0] failed for size %d\n", size);
		exit(1);
	}


	printf("\n");

	ptr[1] = dmalloc(size);
    printf("mallocing ptr[1]: 0x%x\n", ptr[1]);
	if(ptr[1] == NULL)
	{
		printf("malloc of ptr[1] failed for size %d\n", size);
		exit(1);
	}


	printf("\n");

	ptr[2] = dmalloc(size);
    printf("mallocing ptr[2]: 0x%x\n", ptr[2]);
	if(ptr[2] == NULL)
	{
		printf("malloc of ptr[2] failed for size %d\n", size);
		exit(1);
	}


	printf("\n");

	/*
  	 * this one should fail due to rounding
  	 */
	ptr[3] = dmalloc(size);
    printf("mallocing ptr[3]: 0x%x\n", ptr[3]);
	if(ptr[3] == NULL)
	{
		printf("malloc of ptr[3] fails correctly for size %d\n", size);
	}


	printf("\n");

	/*
  	 * free the first block
  	 */
    printf("freeing ptr[0]: 0x%x\n", ptr[0]);
	dfree(ptr[0]);


	printf("\n");

	/*
  	 * free the third block
   	 */
    printf("freeing ptr[2]: 0x%x\n", ptr[2]);
	dfree(ptr[2]);

	printf("\n");


	/*
  	 * now free secoond block
   	 */
    printf("freeing ptr[1]: 0x%x\n", ptr[1]);
	dfree(ptr[1]);

	printf("\n");

	/*
 	 * re-malloc first pointer
  	 */
	ptr[0] = dmalloc(size);
    printf("remallocing ptr[0]: 0x%x\n", ptr[0]);
	if(ptr[0] == NULL)
	{
		printf("re-malloc of ptr[0] failed for size %d\n", size);
		exit(1);
	}
	printf("\n");

	/*
  	 * try splitting the second block
  	 */
	ptr[1] = dmalloc(size/2);
    printf("mallocing ptr[1]: 0x%x with half size\n", ptr[1]);
	if(ptr[1] == NULL)
	{
		printf("split second block ptr[1] failed for size %d\n", size/2);
		exit(1);
	}

	printf("\n");

	/*
  	 * free first block and split of second
 	 */
    printf("freeing ptr[0]: 0x%x\n", ptr[0]);
	dfree(ptr[0]);
    printf("freeing ptr[1] (half size): 0x%x\n", ptr[1]);
	dfree(ptr[1]);

	printf("\n");



	/*
 	 * try mallocing a little less to make sure no split occurs
   	 * first block from previous print should not be split
  	 */
	ptr[0] = dmalloc(size-1);
    printf("remallocing ptr[0]: 0x%x\t with size: %d\n", ptr[0],size-1);
	if(ptr[0] == NULL)
	{
		printf("slightly smaller malloc of ptr[0] failed for size %d\n", size);
		exit(1);
	}
    


	/*
  	 * free it and make sure it comes back as the correct size
  	 */
    printf("freeing ptr[0]: 0x%x\n", ptr[0]);
	dfree(ptr[0]);
	
	printf("\n");

	/*
 	 * okay, now see if multiples work
  	 */
	for(i=0; i < 6; i++)
	{
		ptr[i] = dmalloc(100);
        printf("mallocing ptr[%d]: 0x%x\n", i, ptr[i]);

	}
    
    
    
	/*
 	 * free first block, third block, fifth block
  	 */
    printf("freeing ptr[0]: 0x%x\n", ptr[0]);
	dfree(ptr[0]);

    printf("freeing ptr[2]: 0x%x\n", ptr[2]);
	dfree(ptr[2]);

    printf("freeing ptr[4]: 0x%x\n", ptr[4]);
	dfree(ptr[4]);

	printf("\n");

	/*
 	 * now, free second block -- first, second, third blocks
   	 * should coalesce
	 */
    printf("freeing ptr[1]: 0x%x, blocks 1-3 should coalesce\n", ptr[1]);
	dfree(ptr[1]);

	printf("\n");

	/*
	 * free the sixth block and it shoudl merge with the last
  	 * block leaving two
 	 */
    printf("freeing ptr[5]: 0x%x, should merge with last block so 2 remaining\n", ptr[5]);
	dfree(ptr[5]);

	printf("\n");

	/*
  	 * now free fourth block and they should all be together
  	 */
    printf("freeing ptr[3]: 0x%x, all should be together\n", ptr[3]);
	dfree(ptr[3]);

	printf("\n");

	printf("Stress testcases1 passed!\n");

	exit(0);
}
