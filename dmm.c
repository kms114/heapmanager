#include <stdio.h>  // needed for size_t
#include <unistd.h> // needed for sbrk
#include <assert.h> // needed for asserts
#include "dmm.h"

/* You can improve the below metadata structure using the concepts from Bryant
 * and OHallaron book (chapter 9).
 */

typedef struct metadata {
    /* size_t is the return type of the sizeof operator. Since the size of an
    * object depends on the architecture and its implementation, size_t is used
    * to represent the maximum size of any object in the particular
    * implementation. size contains the size of the data object or the number of
    * free bytes
    */
    size_t size;
    struct metadata* next;
    struct metadata* prev; 
} metadata_t;

/* freelist maintains all the blocks which are not in use;
 */

static metadata_t* freelist = NULL;

bool init = false;
void setFreelistNext(metadata_t* current, metadata_t* value) {
    metadata_t** next = (metadata_t**) ((size_t)current + sizeof(metadata_t) + sizeof(metadata_t*));
    *next = value;
    return;
}

void setFreelistPrev(metadata_t* current, metadata_t* value) {
	metadata_t** prev = (metadata_t**) ((size_t)current + sizeof(metadata_t));
    *prev = value;
    return;
}

// TODO: check these functions once add to list is implemented: 
metadata_t* getFreelistNext(metadata_t* current) {
	// null if end of freelist
	// add size of header plus size of "prev" pointer (because "prev" pointer is stored first)
	metadata_t** next = (metadata_t**) ((size_t)current + sizeof(metadata_t) + sizeof(metadata_t*));
    return *next;
}

metadata_t* getFreelistPrev(metadata_t* current) {
	// null if beg of freelist
	// add size of header
	metadata_t** prev = (metadata_t**) ((size_t)current + sizeof(metadata_t));
	return *prev;
}

size_t alignSize(size_t size) {
	if (size % WORD_SIZE != 0) 
		size += WORD_SIZE - (size % WORD_SIZE);
	return size;
}

size_t getActualSize(size_t userRequested) {
	size_t size = sizeof(metadata_t) + userRequested;
    size_t minSize = (sizeof(metadata_t) + 2*sizeof(metadata_t*));
    if (size < minSize) {
        size = minSize;
    }
	return alignSize(size);
}

bool isFree(metadata_t* current){
    if (current == NULL){
        //printf("null\n");
        return false;
    }
    bool isAllocated = (bool) (current->size & 1);
    return !isAllocated;
}

void addToFreeList(metadata_t* current) {
    //Update allocation bit to 0
    // we should be setting the free list pointers inside the block
    current->size = current->size & (~0 - 1);
    
    if (freelist == NULL) {
        freelist = current;
        setFreelistNext(current, NULL);
        setFreelistPrev(current, NULL);
        return;
    }
    setFreelistNext(current, freelist);
    setFreelistPrev(current, NULL);
    setFreelistPrev(freelist, current);
    freelist = current;

    // if (current->size == 8672){
    //   print_freelist3();
    //   exit(0);
    // }
    // if (freelist == NULL){
      // freelist = current;
    // }
    // else{
      // metadata_t* prevFirst = getFreelistPrev(freelist);
      // prevFirst = current;
      // freelist = current;
    // }
}

void removeFromFreeList(metadata_t* current) {
    //Update allocation bit to 1
    current->size = current->size | 1;

    metadata_t* freelistNext = getFreelistNext(current);
    metadata_t* freelistPrev = getFreelistPrev(current);
    //only block in freelist
    if(freelistNext == NULL && freelistPrev == NULL) {
        freelist = NULL;
        return;
    }

    //first block in freelist
    if(freelistPrev == NULL) {
        freelist = freelistNext;
        setFreelistPrev(freelist, NULL);
        return;
    }

    //last block in freelist
    if(freelistNext == NULL) {
        setFreelistNext(freelistPrev, NULL);
        return;
    }

    //in middle of freelist

    setFreelistNext(freelistPrev, freelistNext);
    setFreelistPrev(freelistNext, freelistPrev);
    // metadata_t* prevNext = getFreelistNext(freelistPrev);
    // prevNext = freelistNext;
    // metadata_t* nextPrev = getFreelistPrev(freelistNext);
    // nextPrev = freelistPrev;


}

bool coalesce(metadata_t* current){
    metadata_t* addressPrev = current->prev;
    metadata_t* addressNext = current->next; 
    bool prevFree = isFree(addressPrev);
    bool nextFree = isFree(addressNext);
    // if neither prev or next blocks are free, can't coalesce
    if (!prevFree && !nextFree){
        return false;
    }
    // need to account for edge cases:
    //      if prev
    // if only prev block is free
    if (prevFree && !nextFree){
        //printf("coalesce prev only\n");
        addressPrev->size = addressPrev->size + current->size;
        addressPrev->next = current->next;
        if (addressPrev->next != NULL) {
            addressPrev->next->prev = addressPrev;
        }
    }
    // if only next block is free
    else if(!prevFree && nextFree){
        current->next = addressNext->next;
        if (current->next != NULL) {
            current->next->prev = current;
        }
        current->size = current->size + addressNext->size;
        
        metadata_t* freelistNext = getFreelistNext(addressNext);
        metadata_t* freelistPrev = getFreelistPrev(addressNext);
        setFreelistNext(current, freelistNext);
        setFreelistPrev(current, freelistPrev);
        
        if (freelistPrev != NULL) {
            // metadata_t* freelistPrevNext = getFreelistNext(freelistPrev);
            // if (freelistPrevNext != NULL) setFreelistNext(freelistPrevNext, current);
            setFreelistNext(freelistPrev, current);
        }
        
        if (freelistNext != NULL) {
            setFreelistPrev(freelistNext, current);
        }
        if (addressNext == freelist) {

            freelist = current;
        }

    }
    else{
        //printf("coalesce both\n");
        // TODO: I'm really not sure if this part is right
        // set addressPrev.freelistNext to addressNext.freelistNext
        // set addressPrev.freelistNext.freelistPrev to addressPrev
        metadata_t* freelistNext = getFreelistNext(addressNext);
        metadata_t* addrNextFreelistPrev = getFreelistPrev(addressNext);
        if (addrNextFreelistPrev != NULL) {
            setFreelistNext(addrNextFreelistPrev, freelistNext);
        } else {
            freelist = getFreelistNext(addressNext);
        }
        
        // if freelistNext isn't null, set to 
        if (freelistNext != NULL) {
            setFreelistPrev(freelistNext, addrNextFreelistPrev);
        }
        // metadata_t* addrPrevFreelistNext = getFreelistNext(addressPrev);
        // if (addrPrevFreelistNext != NULL) {
            // setFreelistPrev(addrPrevFreelistNext, addressPrev);
        // }
        
        addressPrev->size = addressPrev->size + current->size + addressNext->size;
        addressPrev->next = addressNext->next;
        if (addressPrev->next != NULL) {
            addressPrev->next->prev = addressPrev;
        }
        // This was wrong, we need just set freelist prev and freelist next to null.
        //removeFromFreeList(addressNext);
        // do i need to check if this is the front/back of the freelist?
        
    }
    return true;
}

void* dmalloc(size_t numbytes) {
	/* initialize through sbrk call first time */

	if(freelist == NULL) {		
            if(!dmalloc_init()){
            return NULL;
        }
	}

    if (numbytes <= 0){
        return NULL;
    }

	// iterate through free list
	metadata_t* current = freelist;
	metadata_t* free_block = NULL;
	size_t actual_size = getActualSize(numbytes); // requested size + metadata
	while(current != NULL) {
		// look for first sufficiently sized block
		if (current->size >= actual_size) {
			free_block = current;
			break;
		} 
		current = getFreelistNext(current);
	}

	if (free_block == NULL){
        return NULL;
    } 
    // printf("free block address: 0x%x\n", free_block);

   //remove from free list(current);
  

    metadata_t* split_block = NULL;

    // printf("free_block pointer = 0x%x\n", free_block);
	
	// check if we need to split block: 
	// if difference is >= metadata size plus two free list pointers
    if (free_block->size - actual_size >= sizeof(metadata_t) + 2*sizeof(metadata_t*)) {
		// insert header into split_block
        split_block = (metadata_t*) ((size_t)free_block + actual_size);
		// update address pointers & sizes
		split_block->size = free_block->size - actual_size;
		split_block->prev = free_block;
		split_block->next = free_block->next;
		free_block->size = actual_size;
    if (free_block->next != NULL){
        free_block->next->prev = split_block;
    }
		free_block->next = split_block;
	}

    removeFromFreeList(free_block);
    // if (getFreelistPrev(free_block) != NULL){
    //   printf("freelistPrev->next after 2= 0x%x\n", getFreelistNext(getFreelistPrev(free_block)));
    // }
    // if (getFreelistNext(free_block) != NULL){
    //   printf("freelistNext->prev after 2= 0x%x\n", getFreelistPrev(getFreelistNext(free_block)));
    // }
    
    if (split_block != NULL){
        addToFreeList(split_block);
    }

    // if (getFreelistPrev(free_block) != NULL){
    //   printf("freelistPrev->next after 3= 0x%x\n", getFreelistNext(getFreelistPrev(free_block)));
    // }
    // if (getFreelistNext(free_block) != NULL){
    //   printf("freelistNext->prev after 3= 0x%x\n", getFreelistPrev(getFreelistNext(free_block)));
    // }

    void* returnVal = (void*) ((size_t)free_block + sizeof(metadata_t));
    // printf("malloc return: 0x%x\n", returnVal);
    // if (split_block != NULL && split_block->size == 8672){
    //     printf("Printing Freelist at end of Malloc\n");
    //     print_freelist3();
    // }
    return returnVal;
}

void dfree(void* ptr) {
    metadata_t* current = (metadata_t*) ((size_t)ptr - sizeof(metadata_t));
    // if null ptr passed in, do nothing
    if (current == NULL) return;
    
    //Update allocation bit to 0
    current->size = current->size & (~0 - 1);

    // try to coalesce
    if (!coalesce(current)){
        addToFreeList(current);
    }
    // if (current->size == 8672){
    //     printf("Printing Freelist at end of Malloc\n");
    //     print_freelist3();
    // }
    return;
}

bool dmalloc_init() {

  /* Two choices: 
   * 1. Append prologue and epilogue blocks to the start and the
   * end of the freelist 
   *
   * 2. Initialize freelist pointers to NULL
   *
   * Note: We provide the code for 2. Using 1 will help you to tackle the 
   * corner cases succinctly.
   */
  if (init){
    return false;
  }
  size_t max_bytes = ALIGN(MAX_HEAP_SIZE);
  // size_t max_bytes = ALIGN(1024*1024*4);
  /* returns heap_region, which is initialized to freelist */
  freelist = (metadata_t*) sbrk(max_bytes); 
  /* Q: Why casting is used? i.e., why (void*)-1? */

  if (freelist == (void *)-1){
    return false;
  }
	  
  freelist->next = NULL;
  freelist->prev = NULL;
  // TODO: I commented the line below out b/c we're doing our size differently
  //freelist->size = max_bytes-METADATA_T_ALIGNED;
  freelist->size = max_bytes;

  setFreelistNext(freelist, NULL);
  setFreelistPrev(freelist, NULL);
  init = true;

  return true;
}

// void count_freelist() {
//     int count = 0;
//     metadata_t *freelist_head = freelist;
//     while(freelist_head != NULL) {
//         count++; 
//         if (!isFree(freelist_head)) {
//             printf("wrong at count: %d\n", count);
//             print_freelist3();
//             exit(0);
//         }
//         freelist_head = getFreelistNext(freelist_head);
//     }
//     printf("freelist size: %d\n", count);
// }

// void print_freelist3() {
//   metadata_t *freelist_head = freelist;
//   while(freelist_head != NULL) {
//     printf("\tFreelist Size:%zd, Head:%p, Prev: 0x%x, Next: 0x%x\n",
// 	  freelist_head->size,
// 	  freelist_head,
// 	  freelist_head->prev,
// 	  freelist_head->next);
//     printf("\tFreelist Next: 0x%x, Freelist Prev: 0x%x\n",getFreelistNext(freelist_head), getFreelistPrev(freelist_head));
//     freelist_head = getFreelistNext(freelist_head);
//   }
//   int count = 0;
//   freelist_head = freelist;
//   while(freelist_head != NULL) {
//       count++; 
//       freelist_head = getFreelistNext(freelist_head);
//   }
//   printf("freelist size: %d\n", count);
// }

// void print_freelist2() {
//   metadata_t *freelist_head = freelist;
//   int count = 0;
//   while(freelist_head != NULL && count < 10) {
//       count++; 
//     printf("\tFreelist Size:%zd, Head:%p, Prev: 0x%x, Next: 0x%x\n",
// 	  freelist_head->size,
// 	  freelist_head,
// 	  freelist_head->prev,
// 	  freelist_head->next);
//     printf("\tFreelist Next: 0x%x, Freelist Prev: 0x%x\n",getFreelistNext(freelist_head), getFreelistPrev(freelist_head));
//     freelist_head = getFreelistNext(freelist_head);
//   }
//   count = 0;
//   freelist_head = freelist;
//   // printf("bottom below if relevant");
//   while(freelist_head != NULL) {
//       count++; 
//       // printf("\tFreelist Size:%zd, Head:%p, Prev: 0x%x, Next: 0x%x\n",
//       // freelist_head->size,
//       // freelist_head,
//       // freelist_head->prev,
//       // freelist_head->next);
//       // printf("\tFreelist Next: 0x%x, Freelist Prev: 0x%x\n",getFreelistNext(freelist_head), getFreelistPrev(freelist_head));
      
//       if (count >350) {
//           printf("endless loop\n");
//           exit(0);
//       }
//       freelist_head = getFreelistNext(freelist_head);
//   }
//   printf("freelist size: %d\n", count);
// }

/* for debugging; can be turned off through -NDEBUG flag*/
void print_freelist() {
  metadata_t *freelist_head = freelist;
  while(freelist_head != NULL) {
    DEBUG("\tFreelist Size:%zd, Head:%p, Prev:%p, Next:%p\t",
	  freelist_head->size,
	  freelist_head,
	  freelist_head->prev,
	  freelist_head->next);
    freelist_head = freelist_head->next;
  }
  DEBUG("\n");
}
